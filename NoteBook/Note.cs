using System;

namespace NoteBook
{
    public class Note
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string ThirdName { get; set; }
        public long PhoneNumber { get; set; }
        public string Country { get; set; }
        public DateTime TimeBorn { get; set; }
        public string Organization { get; set; }
        public string Work { get; set; }
        public string SomeNote { get; set; }
        public static int count;
        public int Id { get; }

        public Note()
        {
            this.Id = count;
            count += 1;
        }
    }
}