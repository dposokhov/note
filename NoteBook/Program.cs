﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
//Ctrl+Shift+K to push new commit
namespace NoteBook
{
    internal class Program
    {
        static Dictionary<int, Note> maindict = new Dictionary<int, Note>();
        public static void Main(string[] args)
        {
            StartProgram();    
            
        }

        public static void MakeNote(Dictionary<int,Note> maindict)
        {
            Console.Clear();
            Note note = new Note();
            note.SecondName = AnswerOnQuestNonull("Фамилию");
            note.FirstName = AnswerOnQuestNonull("Имя");
            note.ThirdName = AnswerOnQuest("Отчество");
            note.TimeBorn = AnswerOnDate("Дату рождения");
            note.PhoneNumber = AnswerOnNumber("Телефон(от 6 цифр)");
            note.Country = AnswerOnQuestNonull("Страну");
            note.Organization = AnswerOnQuest("Организацию");
            note.Work = AnswerOnQuest("Должность");
            note.SomeNote = AnswerOnQuest("Прочие заметки");
            maindict.Add(note.Id,note);
            Console.Clear();
            Console.WriteLine("-------------------------------");
            Console.WriteLine("Запись создана");
            Console.WriteLine("-------------------------------");
        }
        public static void DeleteNote(Dictionary<int,Note> maindict)
        {
            if (Note.count==0)
            {
                Console.Clear();
                Console.WriteLine("Записей нет. Удалять нечего");
                Console.WriteLine("-------------------------------");
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Введите номер записи от 1 до {0} включительно",Note.count);
                maindict.Remove(GetAnswer(1,Note.count)-1);
                Note.count -= 1;
                Console.Clear();
                Console.WriteLine("-------------------------------");
                Console.WriteLine("Запись удалена");
                Console.WriteLine("-------------------------------");
            }
        }
        public static void WatchNote(Dictionary<int,Note> maindict)
        {
            if (Note.count==0)
            {
                Console.Clear();
                Console.WriteLine("Записей нет. Смотреть нечего");
                Console.WriteLine("-------------------------------");
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Введите номер записи от 1 до {0} включительно",Note.count);
                int answer = GetAnswer(1,Note.count)-1;
                Console.Clear();
                Console.WriteLine("Запись номер {0}:",Note.count);
                Note currentNote = maindict[answer];
                Console.WriteLine("\tФамилия: {0}",currentNote.SecondName);
                Console.WriteLine("\tИмя: {0}",currentNote.FirstName);
                Console.WriteLine("\tОтчество: {0}",currentNote.ThirdName);
                Console.WriteLine("\tДата: {0}",currentNote.TimeBorn);
                Console.WriteLine("\tТелефон: {0}",currentNote.PhoneNumber);
                Console.WriteLine("\tСтрана: {0}",currentNote.Country);
                Console.WriteLine("\tОрганизация: {0}",currentNote.Organization);
                Console.WriteLine("\tДолжность: {0}",currentNote.Work);
                Console.WriteLine("\tПрочие заметки: {0}",currentNote.SomeNote);
                Console.WriteLine("Конец записи");
                Console.WriteLine("-------------------------------");
                Console.WriteLine("Чтобы продолжить нажми любую клавишу");
                Console.ReadKey();
                Console.Clear();
            }
        }
        public static void ShortWatchNote(Dictionary<int,Note> maindict)
        {
            if (Note.count==0)
            {
                Console.Clear();
                Console.WriteLine("Записей нет. Смотреть нечего");
                Console.WriteLine("-------------------------------");
            }
            else
            {
                Console.Clear();
                foreach (var VARIABLE in maindict)
                {
                    Console.Write("Запись номер {0}: ",VARIABLE.Key+1);
                    Console.Write("Фамилия - {0} ",VARIABLE.Value.SecondName);
                    Console.Write("Имя - {0} ",VARIABLE.Value.FirstName);
                    Console.Write("Телефон - {0}",VARIABLE.Value.PhoneNumber);
                    Console.WriteLine(";");
                    
                }
                Console.WriteLine("-------------------------------");
                Console.WriteLine("Чтобы продолжить нажми любую клавишу");
                Console.ReadKey();
                Console.Clear();
            }
        }

        public static int GetAnswer(int number1, int number2)
        {
            Console.WriteLine("Введи число");
            int val;
            while (true)
            {
                try
                {
                    string s = Console.ReadLine();
                    val = Int32.Parse(s);
                    if (!(val>=number1 && val <=number2))
                    {
                        Console.WriteLine("Введите число от {0} до {1}",number1,number2);
                    }
                    else
                    {
                        break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Вы ввели не число. Введи число:");
                }
            }
            return val;
        }

        public static string AnswerOnQuestNonull(string item)
        {
            string answer;
            while (true)
            {
                Console.WriteLine("Введите {0} (Обязательно):",item);
                answer = Console.ReadLine();
                if (answer == "")
                {
                   
                }
                else
                {
                    break;
                }  
            }

            return answer;
        }
        
        public static DateTime AnswerOnDate(string item)
        {
            DateTime answer = DateTime.MinValue;
            while (true)
            {
                try
                {
                    Console.WriteLine("Введите {0} (Не обязательно):",item);
                    string s = Console.ReadLine();
                    if (s == "")
                    {
                        return DateTime.MinValue;
                    }
                    answer = DateTime.Parse(s);
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Это не дата, введи дату(можешь ввести пустую строку):");
                }
            }
            return answer; 
        }
        public static string AnswerOnQuest(string item)
        {
            Console.WriteLine("Введите {0} (Не обязательно):",item);
            string answer = Console.ReadLine();
            return answer;
        }
        
        public static void TypeText()
        {
            Console.WriteLine("Ты можешь\n1-Создать запись \n2-Просмотреть запись\n3-Посмотреть все " +
                              "учетные записи в краткой форме\n4-Редактировать запись\n5-Удалить запись \n6-Выйти ");
        }

        public static long AnswerOnNumber(string item)
        {
            Console.WriteLine("Введите {0} (Обязательно):",item);
            long val;
            while (true)
            {
                try
                {
                    string s = Console.ReadLine();
                    val = long.Parse(s);
                    if (!(val>=100000))
                    {
                        Console.WriteLine("Введите число от 6 цифр");
                    }
                    else
                    {
                        break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Вы ввели не число. Введи число:");
                }
            }
            return val;
        }

        public static void EditNote(Dictionary<int, Note> maindict)
        {
            if (Note.count == 0)
            {
                Console.WriteLine("Записей нет. Редактировать нечего");
            }
            else
            {
                Console.WriteLine("Введите номер записи от 1 до {0} включительно", Note.count);
                int answer = GetAnswer(1,Note.count)-1;
                Console.WriteLine("Выбирете поле записи для редактирования:\n" +
                                  "1-Фамилия\n" +
                                  "2-Имя\n" +
                                  "3-Отчество\n" +
                                  "4-Дата\n" +
                                  "5-Телефон\n" +
                                  "6-Страна\n" +
                                  "7-Организация\n" +
                                  "8-Должность\n" +
                                  "9-Прочие заметки");
                int answer2 = GetAnswer(1,9)-1;
                Console.WriteLine("Запись номер {0}:", Note.count);
                Note note = maindict[answer];
                switch (answer2)
                {
                    case 0:
                    {
                        Console.Clear();
                        note.SecondName = AnswerOnQuestNonull("Фамилию");
                        break;
                    }
                    case 1:
                    {
                        Console.Clear();
                        note.FirstName = AnswerOnQuestNonull("Имя");
                        break;
                    }
                    case 2:
                    {
                        Console.Clear();
                        note.ThirdName = AnswerOnQuest("Отчество");
                        break;
                    }
                    case 3:
                    {
                        Console.Clear();
                        note.TimeBorn = AnswerOnDate("Дату рождения");
                        break;
                    }
                    case 4:
                    {
                        Console.Clear();
                        note.PhoneNumber = AnswerOnNumber("Телефон(от 6 цифр)");
                        break;
                    }
                    case 5:
                    {
                        Console.Clear();
                        note.Country = AnswerOnQuestNonull("Страну");
                        break;
                    }
                    case 6:
                    {
                        Console.Clear();
                        note.Organization = AnswerOnQuest("Организацию");
                        break;
                    }
                    case 7:
                    {
                        Console.Clear();
                        note.Work = AnswerOnQuest("Должность");
                        break;
                    }
                    case 8:
                    {
                        Console.Clear();
                        note.SomeNote = AnswerOnQuest("Прочие заметки");
                        break;
                    }
                }

                Console.WriteLine("Запись отредактирована");
                Console.WriteLine("-------------------------------");
            }
        }

        public static void StartProgram()
        {
            bool exit = false;
            while (true)
            {
                if (exit)
                {
                    break;
                }
                
                TypeText();
                int firstAnswer = GetAnswer(1,6);
                switch (firstAnswer)
                {
                    case 1:
                    {
                        MakeNote(maindict);
                        break;
                    }
                    case 2:
                    {
                        WatchNote(maindict);
                        break;
                    }
                    case 3:
                    {
                        ShortWatchNote(maindict);
                        break;
                    }
                    case 4:
                    {
                        EditNote(maindict);
                        break;
                    }
                    case 5:
                    {
                        DeleteNote(maindict);
                        break;
                    }
                    case 6:
                    {
                        exit = true;
                        break;
                    }
                }
            }
        }
    }
}